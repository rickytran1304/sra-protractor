import { LoginPage } from './login.po';
import { browser, element, by } from "protractor";

export class DashBoardPage {
        
        loginPage = new LoginPage();
        startNewApplicationLink = element(by.id("formio-htmlelement-FCERT"));

        async navigateTo() {
                browser.waitForAngularEnabled(false);
                browser.get("/");
                
                await this.loginPage.login();
        }

        async logOut() {
                this.loginPage.logout();
        }
}