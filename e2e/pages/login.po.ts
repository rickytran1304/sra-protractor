import { ElementHelper } from '../helpers/element_helper';
import { browser, element, by } from "protractor";


export class LoginPage {
        
        nameInput = element(by.id("logonIdentifier"));
        passwordInput = element(by.id("password"));
        submit = element(by.id("next"));

        navigateTo() {
                browser.get("/");
        }

        async login(user=browser.params.login) {

                await browser.get("/");

                ElementHelper.waitForElements([this.nameInput, this.passwordInput, this.submit]);

                await this.nameInput.sendKeys(user.username);
                await this.passwordInput.sendKeys(user.password);
                await this.submit.click();
        }

        async logout() {
                element(by.css('[ng-click="$ctrl.logout()"]')).click();
                ElementHelper.waitForElements([this.nameInput, this.passwordInput, this.submit]);
        }
}