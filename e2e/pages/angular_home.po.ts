import {browser, element, by} from "protractor";

export class AngularHomePage {
        nameInput = element(by.model("yourName"));
        greeting = element(by.binding("yourName"));

        get() {
                browser.get("http://www.angularjs.org");
        }

        setName(name:string) {
                this.nameInput.sendKeys(name);
        }

        getGreetingText(): any {
                return this.greeting.getText();
        }
}