import {browser, element, by} from "protractor";

export class AngularHomePageAsync {
        nameInput = element(by.model("yourName"));
        greeting = element(by.binding("yourName"));

        async get() {
                await browser.get("http://www.angularjs.org");
        }

        async setName(name:string) {
                await this.nameInput.sendKeys(name);
        }

        async getGreetingText(): Promise<any> {
                const text = await this.greeting.getText();
                return text;
        }
}