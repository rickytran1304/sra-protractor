import { DashBoardPage } from '../../pages/dashboard.po';
import { ElementHelper } from '../../helpers/element_helper';
import { browser, element, by } from 'protractor';

describe("Anthenticated user with AC and AWE-PERSON attributes", function() {

        it("should be able to apply for Practicing Certificate", async function(){
                
                const dashBoardPage = new DashBoardPage();
                dashBoardPage.navigateTo();

                browser.sleep(5000);
                browser.get("/pc_start_application_page");
                
                // pc_start_application_page
                ElementHelper.waitForElement(element(by.id("formio-htmlelement-content7")));
                ElementHelper.clickButton("submit");
                browser.sleep(5000);
                
                // pc_application_detail_page
                // Are you subject to any of the circumstances in regulation 3?
                ElementHelper.waitForElement(element(by.id("formio-htmlelement-html3")));
                ElementHelper.scrollToBottom();
                element.all(by.css(".ng-binding.ng-scope")).last().click();
                browser.sleep(2000);
                element(by.id("saveAndContinue")).click();
                browser.sleep(5000);

                // pc_application_detail_page
                // Continuing competence
                ElementHelper.waitForElement(element(by.id("formio-htmlelement-html3")));
                ElementHelper.scrollToBottom();
                element.all(by.css(".ng-binding.ng-scope")).get(1).click();
                browser.sleep(2000);
                element(by.id("saveAndContinue")).click();
                browser.sleep(5000);
                
                // pc_application_detail_page
                // Reduced fees
                ElementHelper.waitForElement(element(by.id("formio-htmlelement-html7")));
                ElementHelper.scrollToBottom();
                element.all(by.css(".ng-binding.ng-scope")).last().click();
                browser.sleep(2000);
                element(by.id("saveAndContinue")).click();
                browser.sleep(5000);

                // pc_application_detail_page
                // Summary
                ElementHelper.waitForElement(element(by.id("formio-htmlelement-html4")));
                ElementHelper.scrollToBottom();
                browser.sleep(2000);
                element(by.id("saveAndContinue")).click();
                browser.sleep(5000);

                // pc_application_detail_page
                ElementHelper.waitForElement(element(by.id("formio-htmlelement-html2")));
                ElementHelper.scrollToBottom();
                element.all(by.css(".ng-binding.ng-scope")).last().click();
                browser.sleep(2000);
                element(by.id("submit")).click();
                browser.sleep(5000);
                
                // pc_application_payment_page
                ElementHelper.waitForElement(element(by.css(".title-fee-summary")));
                ElementHelper.scrollToBottom();
                element(by.id("proceedToPayment")).click();
                browser.sleep(5000);

                // modal proceed payment
                element(by.css("button[ng-click='$ctrl.submitToWP(BuyForm)']")).click();
                browser.sleep(5000);

                // https://secure-test.worldpay.com/wcc/purchase
                expect(await browser.getTitle()).toEqual("Welcome to WorldPay");
        });
});