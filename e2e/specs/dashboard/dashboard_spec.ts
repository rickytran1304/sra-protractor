import { LoginPage } from "../../pages/login.po";
import { browser } from "protractor";

const loginPage = new LoginPage();

describe("Dash board page", function() {

        describe("Authenticate", function(){

                it("should login if provided the proper credential", async function(){
                        await loginPage.login();
                        expect(await browser.driver.getTitle()).toEqual("mySRA");
                });

        });
})