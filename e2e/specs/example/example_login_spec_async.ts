import { AngularHomePageAsync } from '../../pages/angular_home_async.po';

const homePage = new AngularHomePageAsync();

describe("angularjs homepage", function() {

        it("should greet the named user", async function(){
                await homePage.get();

                await homePage.setName("Ricky");

                expect(await homePage.getGreetingText()).toEqual("Hello Ricky!");
        });
})