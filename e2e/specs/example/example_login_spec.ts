import { AngularHomePage } from '../../pages/angular_home.po';

const homePage = new AngularHomePage();

describe("angularjs homepage", function() {

        it("should greet the named user", function(){
                homePage.get();

                homePage.setName("Ricky");

                expect(homePage.getGreetingText()).toEqual("Hello Ricky!");
        });
})