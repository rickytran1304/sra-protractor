import { browser, protractor, element, by } from "protractor";

export class ElementHelper {
        
        static timeout = 10000;

        static waitForElement(element: any, message: string = null) {
                return browser.wait(protractor.ExpectedConditions.elementToBeClickable(element), ElementHelper.timeout, message);
        }
        
        static waitForElements(elements: Array<any>, message:string = null) {
                elements.forEach(element => this.waitForElement(element, message));
        }

        static waitForTextInElement(text:string, element:any) {
                return browser.wait(protractor.ExpectedConditions.textToBePresentInElement(element, text), ElementHelper.timeout);
        }

        static scrollTo(location:number) {
                browser.executeScript(`window.scrollTo(0,${location});`);
        }

        static scrollToBottom() {
                browser.executeScript('window.scrollTo(0,100000);');
        }

        static scrollToElement(element:any) {
                browser.actions().mouseMove(element).perform();
        }

        static clickButton(buttonId: string) {
                ElementHelper.scrollToBottom();
                let submit = element(by.id(buttonId));
                ElementHelper.scrollToElement(submit);
                submit.click();
        }
}