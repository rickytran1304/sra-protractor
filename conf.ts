import { SpecReporter } from 'jasmine-spec-reporter';
import { Config, browser } from "protractor";
import environments from "./environment";

require('dotenv').config();

var fs = require("fs-extra");
const node_env = process.env.ENVIRONMENT || "preprod";
const env = environments[node_env];
const seleniumUrlAddress = process.env.SELENIUM_ADDRESS;
const protractor_username = process.env.PROTRACTOR_USERNAME;
const protractor_password = process.env.PROTRACTOR_PASSWORD;
const isParallel = process.env.PARALLEL === "true";

console.log(`ENVIRONMENT: ${node_env} IS_PARALLEL: ${isParallel} USERNAME: ${protractor_username} PASSWORD: ${protractor_password}`);

let capabilities: any = {
  browserName: "chrome",
  chromeOptions: {
    args: [ 
      "--headless",
      "--incognito",
      "--disable-gpu",
      "--disable-web-security",
      "--disable-extensions",
      "--window-size=1920,1080"
    ]
  }
}

if(isParallel) {
  capabilities = {
    ...capabilities,
    maxInstances: 4,
    shardTestFiles: true
  };
}

export let config: Config = {
        seleniumAddress: seleniumUrlAddress,
        allScriptsTimeout: 10 * 1000, // 10 seconds
        getPageTimeout: 30 * 1000, // 30 seconds
        capabilities,
        baseUrl: env.baseUrl,
        params: { 
          login: { 
            username: protractor_username, 
            password: protractor_password 
          } 
        },
        noGlobals: true,
        framework: "jasmine",
        specs: [ 
          "e2e/specs/**/*.js"
        ],
        jasmineNodeOpts: {
          showColors: true,
          showTiming: true,
          defaultTimeoutInterval: 120000,
          isVerbose: true,
          includeStackTrace: true
        },
        beforeLaunch: function(){
          // clean up any leftover from previous run
        },
        onPrepare: function(){
          browser.ignoreSynchronization = true;

          jasmine.getEnv().addReporter(new SpecReporter({
            spec: {
              displayStacktrace: true
            }
          }));
        }
    };