### Description
- SRA has a heavy workflow process and lots of business flows with user interactions that may changes a lot during development process, that's why we should have automation test to cover it, so that we can guarantee new changes won't break anything
- Protractor is built on top of TypeScript so that we can easily organize and set up for test.

### Why Protractor

- It is very stable and mature (created 6 years ago) [Compare Tables](https://npmcompare.com/compare/nightwatch, protractor, webdriverio, dalekjs)
- It can be used not only for Angular but also for React because it is based on **WebDriverJS**
- It is well-documented [Document Here](http://www.protractortest.org/#/api)
- It is built on top of **TypeScript**

### Technologies

- Language: **TypeScript**
- Test Runner: **Protractor**
- Test Framework: **Jasmine**

### How to Run

#### Prerequisites

- *Environment*: MacOS or Windows 8/10
- *IDE*: VSCode
- *node*: 8.11.4
- *npm*: 5.3

#### Project Setup

- `npm install`

#### Dev Environment

- It can run very well on Windows 10
- It currently support all major versions of Chrome, Firefox, Safari and IE
- It will run on **PREPROD** with the default **user/password** for protractor

#### Run

-----

| Command | Environment |
|---------|-------------|
| `npm test` | https://preprod-temp-gateway.azurewebsites.net |
| `npm test-parallel` | https://preprod-temp-gateway.azurewebsites.net/ |

-----

### Style Guides

- *environment.ts* is a file contains all the `user/password`
- Use *async/await* in every protractor API to make the code running in sequence
- Define a **PageObject** and call it in **Spec**

### TypeScript Known Issues

- https://github.com/Microsoft/TypeScript/issues/9448

### Author

- Tran Nhat Hoang, SRA Team

### Main Contributors

- Vo Hoai Thanh Phuong, SRA Team
- Duong Khac Trinh, SRA Team
- Duong Thanh Liem, SRA Team